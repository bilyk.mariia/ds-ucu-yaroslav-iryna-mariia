# Distributed systems UCU - Yaroslav Iryna Mariia Team

## Team
<p> - Yaroslav Borodaienko</p>
<p> - Iryna Kostyshyn</p>
<p> - Mariia Bilyk</p>

## Installation
1. Pull project
2. Go to repo root folder and run in terminal `docker-compose -f docker-compose.yml up` 
3. Test scenarios is [here](https://www.getpostman.com/collections/f5dd685cd9d05f3be84a).
4. Master api available at http://localhost:50052/messages. To append message you should invoke POST.
5. Secondaries available at http://localhost:8001/messages and http://localhost:50050/messages. To get all messages you should invoke GET.

## Latest changes contains fuctionality of 2nd lab
Added POST endpoint to master, exsamples are [here](https://www.getpostman.com/collections/f5dd685cd9d05f3be84a) 
