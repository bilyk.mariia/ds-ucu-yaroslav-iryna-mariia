﻿using ReplicatedLog.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplicatedLog.BuildingBlocks.Storage.InMemory
{
    public class MessageReplicationStatusStorage : IMessageReplicationStatusStorage
    {
        private readonly List<MessageReplicationStatus> _messagesStatusesList = new List<MessageReplicationStatus>();

        public void Append(MessageReplicationStatus item)
        {
            _messagesStatusesList.Add(item);
        }

        public List<MessageReplicationStatus> GetAll()
        {
            return _messagesStatusesList
                .OrderByDescending(i => i.MessageId)
                .ToList();
        }

        public MessageReplicationStatus GetLast()
        {
            return _messagesStatusesList.LastOrDefault();
        }

        public bool AnyToReplicate()
        {
            return _messagesStatusesList
                .GroupBy(i => (i.MessageId, i.SecondaryName))
                .Any(i => !i.Any(m => m.ReplicatedSuccess));
        }

        public IEnumerable<MessageReplicationStatus> GetAllToReplicate()
        {
            return _messagesStatusesList
                .GroupBy(i => (i.MessageId, i.SecondaryName))
                .Where(i => !i.Any(m => m.ReplicatedSuccess))
                .SelectMany(group => group);
        }
    }
}
