﻿using System.Collections.Generic;
using System.Linq;
using ReplicatedLog.Domain;

namespace ReplicatedLog.BuildingBlocks.Storage.InMemory;
public class MessageStorage : IMessageStorage
{
    private readonly Dictionary<long, Message> _messagesDict = new Dictionary<long, Message>();

    public void Append(Message item)
    {
        _messagesDict.Add(item.Id, item);
    }

    public List<Message> GetAll()
    {
        return _messagesDict.Values
            .OrderByDescending(i => i.Id)
            .ToList();
    }

    public Message GetLast()
    {
        return _messagesDict.LastOrDefault().Value;
    }

    public Message Get(long key)
    {
        return _messagesDict.GetValueOrDefault(key);
    }
}
