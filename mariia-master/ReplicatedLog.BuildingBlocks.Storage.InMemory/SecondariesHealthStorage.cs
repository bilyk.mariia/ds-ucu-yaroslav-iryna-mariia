﻿using System.Collections.Generic;
using System.Linq;
using ReplicatedLog.Domain;

namespace ReplicatedLog.BuildingBlocks.Storage.InMemory;

public class SecondariesHealthStorage : ISecondariesHealthStorage
{
    private readonly Dictionary<string, SecondaryHealth> _Dict = new Dictionary<string, SecondaryHealth>();

    public void Append(SecondaryHealth item)
    {
        if (!_Dict.TryAdd(item.SecondaryName, item))
            _Dict[item.SecondaryName] = item;
    }

    public bool CheckQuorum()
    {
        return _Dict.Values.Count(h => h.IsHealthy) + 1 > _Dict.Values.Count(h => !h.IsHealthy);
    }

    public List<SecondaryHealth> GetAll()
    {
        return _Dict.Values.ToList();
    }

    public SecondaryHealth GetLast()
    {
        throw new System.NotImplementedException();
    }

    public bool IsHealthy(string secondaryName)
    {
        if (_Dict.TryGetValue(secondaryName, out var secondaryHealth) )
            return secondaryHealth.IsHealthy;

        return false;
    }
}
