﻿using ReplicatedLog.Domain;
using System.Collections.Generic;

namespace ReplicatedLog.BuildingBlocks.Storage;

public interface IMessageReplicationStatusStorage: IStorage<MessageReplicationStatus>
{
    IEnumerable<MessageReplicationStatus> GetAllToReplicate();
    bool AnyToReplicate();
}
