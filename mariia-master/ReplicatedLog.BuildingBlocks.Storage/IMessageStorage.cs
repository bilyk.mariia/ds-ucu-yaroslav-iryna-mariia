﻿using ReplicatedLog.Domain;

namespace ReplicatedLog.BuildingBlocks.Storage;
public interface IMessageStorage : IStorage<Message>
{
    Message Get(long key);
}
