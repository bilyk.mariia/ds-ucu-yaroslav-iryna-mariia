﻿using ReplicatedLog.Domain;

namespace ReplicatedLog.BuildingBlocks.Storage;

public interface ISecondariesHealthStorage : IStorage<SecondaryHealth>
{
    bool IsHealthy(string secondaryName);
    bool CheckQuorum();
}
