﻿using System.ComponentModel.DataAnnotations;

namespace ReplicatedLog.Domain;
public class CreateMessageModel
{
    [Required]
    public string MessageText { get; set; }
}

