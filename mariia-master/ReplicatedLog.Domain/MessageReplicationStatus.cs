﻿namespace ReplicatedLog.Domain
{
    public class MessageReplicationStatus
    {
        public long MessageId { get; set; }
        public string SecondaryName { get; set; }
        public bool ReplicatedSuccess { get; set; }
        public int ReplicationRetry { get; set; }

        public override string ToString()
        {
            return $"{MessageId} | {SecondaryName} | {ReplicatedSuccess} | {ReplicationRetry}";
        }
    }
}
