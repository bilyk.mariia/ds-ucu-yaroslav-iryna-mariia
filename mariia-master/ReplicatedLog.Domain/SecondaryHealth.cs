﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplicatedLog.Domain
{
    public class SecondaryHealth
    {
        public string SecondaryName { get; set; }
        public bool IsHealthy { get; set; }
        public DateTime LastCheck { get; set; }
    }
}
