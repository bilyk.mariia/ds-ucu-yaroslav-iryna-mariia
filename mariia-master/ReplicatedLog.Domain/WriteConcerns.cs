﻿namespace ReplicatedLog.Domain;

public enum WriteConcerns
{
    InstantReturn = 0,
    MasterOnly = 1,
    Majority = 2,
    All = 3
}
