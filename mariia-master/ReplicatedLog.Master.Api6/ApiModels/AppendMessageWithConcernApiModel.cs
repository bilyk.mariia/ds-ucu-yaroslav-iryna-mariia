﻿using ReplicatedLog.Domain;

namespace ReplicatedLog.Master.Api6.ApiModels;

public class AppendMessageWithConcernApiModel: MessageApiModel
{
    public WriteConcerns WriteConcern { get; set; }   
}
