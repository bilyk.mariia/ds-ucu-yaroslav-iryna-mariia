﻿using System.ComponentModel.DataAnnotations;

namespace ReplicatedLog.Master.Api6.ApiModels;

public class MessageApiModel
{
    [Required]
    public string? MessageText { get; set; }
}
