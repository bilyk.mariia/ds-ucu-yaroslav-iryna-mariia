﻿using MediatR;

namespace ReplicatedLog.Master.Api6.Commands
{
    public class CompleteReplicationCommand: IRequest
    {
        public string Secondary { get; set; }

        public CompleteReplicationCommand(string secondary)
        {
            Secondary = secondary;
        }
    }
}
