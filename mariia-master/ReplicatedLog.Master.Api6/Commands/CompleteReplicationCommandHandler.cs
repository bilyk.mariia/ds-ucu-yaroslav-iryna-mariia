﻿using MediatR;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.Services.Interfaces;
using ReplicatedLog.Master.Api6.Settings;

namespace ReplicatedLog.Master.Api6.Commands
{
    public class CompleteReplicationCommandHandler : AsyncRequestHandler<CompleteReplicationCommand>
    {
        private readonly IMessageStorage messageStorage;
        private readonly IMessageReplicationStatusStorage messageReplicationStatusStorage;
        private readonly ISecondariesHealthStorage secondariesHealthStorage;
        private readonly IConfiguration configuration;
        private readonly ISendMessageToSecondariesService sendMessageToSecondariesService;
        private readonly ILogger<CompleteReplicationCommandHandler> logger;

        public CompleteReplicationCommandHandler(IMessageStorage messageStorage,
            IMessageReplicationStatusStorage messageReplicationStatusStorage,
            ISecondariesHealthStorage secondariesHealthStorage,
            IConfiguration configuration,
            ISendMessageToSecondariesService sendMessageToSecondariesService,
            ILogger<CompleteReplicationCommandHandler> logger)
        {
            this.messageStorage = messageStorage;
            this.messageReplicationStatusStorage = messageReplicationStatusStorage;
            this.secondariesHealthStorage = secondariesHealthStorage;
            this.configuration = configuration;
            this.sendMessageToSecondariesService = sendMessageToSecondariesService;
            this.logger = logger;
        }

        protected override async Task Handle(CompleteReplicationCommand request, CancellationToken cancellationToken)
        {
            logger.LogInformation("CompleteReplication: process started.");
            if (!messageReplicationStatusStorage.AnyToReplicate())
            {
                logger.LogInformation("CompleteReplication: there is no messages to replicate.");
                return;
            }

            var secondarySettings = configuration
                .GetSection(SettingsConstants.ReplicationSecondariesSettingsName)
                .Get<List<ReplicationSecondarySettings>>()
                .FirstOrDefault(s => s.Name == request.Secondary);

            if (!secondariesHealthStorage.IsHealthy(request.Secondary) || secondarySettings == null)
            {
                logger.LogInformation("CompleteReplication: there is no helthy secondary to replicate.");
                return;
            }
            
            var messagesToCompleteReplication = messageReplicationStatusStorage
                .GetAllToReplicate()
                .GroupBy(i => i.MessageId)
                .ToList();

            if (!messagesToCompleteReplication.Any()) 
                logger.LogInformation("CompleteReplication: there is no messages to replicate.");

            foreach (var messageLog in messagesToCompleteReplication)
            {
                var message = messageStorage.Get(messageLog.Key);

                logger.LogInformation($"CompleteReplication: needed replication for message {messageLog.Key} " +
                    $"to secondaries {secondarySettings}.");

                var result = await sendMessageToSecondariesService
                    .SendMessageToSecondaries(message, WriteConcerns.MasterOnly, new List<ReplicationSecondarySettings> { secondarySettings});

                logger.LogInformation($"CompleteReplication: succeed replication for message {messageLog.Key} " +
                    $"to secondaries {secondarySettings}.");
            }
        }
    }
}
