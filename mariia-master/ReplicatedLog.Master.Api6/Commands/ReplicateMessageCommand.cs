﻿using MediatR;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.Services;
using ReplicatedLog.Master.Api6.Settings;
using ReplicatedLogGrcp;

namespace ReplicatedLog.Master.Api6.Commands
{
    public class ReplicateMessageCommand: IRequest<MessageReplicationStatus>
    {
        public ReplicateMessageCommand(AppendMessageRequest message, ReplicationSecondarySettings secondarySettings)
        {
            MessageRequestModel = message;
            SecondarySettings = secondarySettings;
        }

        public AppendMessageRequest MessageRequestModel { get; }
        public ReplicationSecondarySettings SecondarySettings { get; }
    }
}
