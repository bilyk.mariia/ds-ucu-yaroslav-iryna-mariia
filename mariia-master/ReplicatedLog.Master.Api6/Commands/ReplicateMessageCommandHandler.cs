﻿using Grpc.Core;
using Grpc.Net.ClientFactory;
using MediatR;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.Settings;
using ReplicatedLogGrcp;

namespace ReplicatedLog.Master.Api6.Commands
{
    public class ReplicateMessageCommandHandler : IRequestHandler<ReplicateMessageCommand, MessageReplicationStatus>
    {
        private readonly GrpcClientFactory grpcClientFactory;
        private readonly ILogger<ReplicateMessageCommandHandler> logger;
        private readonly ISecondariesHealthStorage secondariesHealthStorage;
        private readonly IMessageReplicationStatusStorage replicationStatusStorage;

        public ReplicateMessageCommandHandler(GrpcClientFactory grpcClientFactory,
            ILogger<ReplicateMessageCommandHandler> logger, ISecondariesHealthStorage secondariesHealthStorage,
            IMessageReplicationStatusStorage replicationStatusStorage)
        {
            this.grpcClientFactory = grpcClientFactory;
            this.logger = logger;
            this.secondariesHealthStorage = secondariesHealthStorage;
            this.replicationStatusStorage = replicationStatusStorage;
        }

        public async Task<MessageReplicationStatus> Handle(ReplicateMessageCommand request, CancellationToken cancellationToken)
        {
            MessageReplicationStatus result;
            if (!secondariesHealthStorage.IsHealthy(request.SecondarySettings.Name))
            {
                var log = new MessageReplicationStatus
                {
                    MessageId = request.MessageRequestModel.Id,
                    SecondaryName = request.SecondarySettings.Name,
                    ReplicatedSuccess = false,
                    ReplicationRetry = 1
                };
                logger.LogInformation(log.ToString());
                SaveLog(log);
                return log;
            }

            logger.LogInformation("ReplicateMessageCommandHandler started");
            var client = grpcClientFactory.CreateClient<MessageReplication.MessageReplicationClient>(request.SecondarySettings.Name);

            try
            {
                var call = client.AppendMessageAsync(request.MessageRequestModel );
                Metadata headers = await call.ResponseHeadersAsync;
                int.TryParse(headers.GetValue("grpc-previous-rpc-attempts"), out var retry);

                AppendMessageReply response = await call.ResponseAsync;
                result = new MessageReplicationStatus
                {
                    MessageId = request.MessageRequestModel.Id,
                    SecondaryName = request.SecondarySettings.Name,
                    ReplicatedSuccess = response.Success,
                    ReplicationRetry = retry
                };
            }
            catch (Exception e)
            {
                //LogError(request.SecondarySettings, e.Message);
                result = new MessageReplicationStatus
                {
                    MessageId = request.MessageRequestModel.Id,
                    SecondaryName = request.SecondarySettings.Name,
                    ReplicatedSuccess = false,
                    ReplicationRetry = -1
                };
            }
            
            logger.LogInformation(result.ToString());
            SaveLog(result);
            return result;
        }

        private void SaveLog(MessageReplicationStatus status)
        {
            replicationStatusStorage.Append(status);
        }

        private void LogError(ReplicationSecondarySettings replicationSecondary, string errorMessage)
        {
            logger.LogError(
                $"Call to secondary with name:{replicationSecondary.Name} failed with error {errorMessage}.");
        }
    }
    }
