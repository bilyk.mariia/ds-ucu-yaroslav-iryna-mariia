﻿using MediatR;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.ApiModels;

namespace ReplicatedLog.Master.Api6.Commands
{

    public class SaveMessageCommand : SaveMessageCommandBase, IRequest<bool>
    {
        public SaveMessageCommand(string? messageText, WriteConcerns writeConcern = WriteConcerns.All) 
            : base(messageText, writeConcern)
        {
        }
    }
}
