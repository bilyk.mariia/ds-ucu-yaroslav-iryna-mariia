﻿using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.ApiModels;

namespace ReplicatedLog.Master.Api6.Commands
{
    public class SaveMessageCommandBase
    {
        public string? MessageText { get; set; }
        public WriteConcerns WriteConcern { get; set; }

        public SaveMessageCommandBase(string? messageText, WriteConcerns writeConcern = WriteConcerns.All)
        {
            MessageText = messageText;
            WriteConcern = writeConcern;
        }
    }
}
