﻿using MediatR;
using ReplicatedLog.Master.Api6.Services.Interfaces;
using ReplicatedLog.Master.Api6.Settings;

namespace ReplicatedLog.Master.Api6.Commands
{
    public class SaveMessageCommandHandler: IRequestHandler<SaveMessageCommand, bool>
    {
        private readonly ISaveMessageService saveMessageService;
        private readonly IConfiguration configuration;
        private readonly ISendMessageToSecondariesService sendMessageToSecondariesService;
        private readonly ILogger<SaveMessageCommandHandler> logger;

        public SaveMessageCommandHandler(ISaveMessageService saveMessageService,
            ILogger<SaveMessageCommandHandler> logger,
            IConfiguration configuration,
            ISendMessageToSecondariesService sendMessageToSecondariesService)
        {
            this.saveMessageService = saveMessageService;
            this.configuration = configuration;
            this.sendMessageToSecondariesService = sendMessageToSecondariesService;
            this.logger = logger;
        }

        public async Task<bool> Handle(SaveMessageCommand request, CancellationToken cancellationToken)
        {
            var messageModel = await saveMessageService.SaveMessageAsync(request.MessageText);
            logger.LogInformation($"Message with id {messageModel.Id} appended to master db.");

            var secondariesSettings = configuration
                .GetSection(SettingsConstants.ReplicationSecondariesSettingsName)
                .Get<List<ReplicationSecondarySettings>>();

            var saveModelToSecondariesSuccess = await sendMessageToSecondariesService
                .SendMessageToSecondaries(messageModel, request.WriteConcern, secondariesSettings);

            return saveModelToSecondariesSuccess;
        }
    }
}
