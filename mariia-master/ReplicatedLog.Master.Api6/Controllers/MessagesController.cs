using MediatR;
using Microsoft.AspNetCore.Mvc;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.ApiModels;
using ReplicatedLog.Master.Api6.Commands;
using ReplicatedLog.Master.Api6.Jobs;
using ReplicatedLog.Master.Api6.Services;
using ReplicatedLog.Master.Api6.Services.Interfaces;

namespace ReplicatedLog.Master.Api6.Controllers;

[ApiController]
[Route("[controller]")]
public class MessagesController : ControllerBase
{
    private readonly ILogger<MessagesController> _logger;
    private readonly IMessageStorage _messageStorage;
    private readonly IMediator mediator;
    private readonly HealthCheckJob healthCheckJob;
    private readonly ISecondariesHealthStorage secondariesHealthStorage;

    public MessagesController(ILogger<MessagesController> logger, IMessageStorage messageStorage, IMediator mediator, HealthCheckJob healthCheckJob,
        ISecondariesHealthStorage secondariesHealthStorage)
    {
        _logger = logger;
        _messageStorage = messageStorage;
        this.mediator = mediator;
        this.healthCheckJob = healthCheckJob;
        this.secondariesHealthStorage = secondariesHealthStorage;
    }

    [HttpPost]
    public async Task<IActionResult> AppendMessage(MessageApiModel model)
    {
        if (!secondariesHealthStorage.CheckQuorum()) return StatusCode(503);
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var result = await mediator.Send(new SaveMessageCommand(model.MessageText));
        return !result ? StatusCode(500) : Ok();
    }

    [HttpPost("writeConcern")]
    public async Task<IActionResult> AppendMessageWithWriteConcern(AppendMessageWithConcernApiModel model)
    {
        if (!secondariesHealthStorage.CheckQuorum()) return StatusCode(503);
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (model.WriteConcern == WriteConcerns.InstantReturn)
        {
            mediator.Send(new SaveMessageCommand(model.MessageText, model.WriteConcern));
            _logger.LogInformation("Processing of message enqueued.");
            return Ok();
        }

        var result = await mediator.Send(new SaveMessageCommand(model.MessageText, model.WriteConcern));
        return !result ? StatusCode(500) : Ok();
    }

    [HttpGet]
    public IActionResult GetMessages()
    {
        var messages = _messageStorage.GetAll();
        return Ok(messages);
    }

    [HttpGet("checkSecondaries")]
    public async Task<IActionResult> CheckSecondaries()
    {
        return Ok(secondariesHealthStorage.GetAll());
    }
}
