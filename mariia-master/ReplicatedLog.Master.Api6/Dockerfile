#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["ReplicatedLog.Master.Api6/ReplicatedLog.Master.Api6.csproj", "ReplicatedLog.Master.Api6/"]
COPY ["ReplicatedLog.BuildingBlocks.Storage/ReplicatedLog.BuildingBlocks.Storage.csproj", "ReplicatedLog.BuildingBlocks.Storage/"]
COPY ["ReplicatedLog.Domain/ReplicatedLog.Domain.csproj", "ReplicatedLog.Domain/"]
COPY ["ReplicatedLog.BuildingBlocks.Storage.InMemory/ReplicatedLog.BuildingBlocks.Storage.InMemory.csproj", "ReplicatedLog.BuildingBlocks.Storage.InMemory/"]
RUN dotnet restore "ReplicatedLog.Master.Api6/ReplicatedLog.Master.Api6.csproj"
COPY . .
WORKDIR "/src/ReplicatedLog.Master.Api6"
RUN dotnet build "ReplicatedLog.Master.Api6.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ReplicatedLog.Master.Api6.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ReplicatedLog.Master.Api6.dll"]