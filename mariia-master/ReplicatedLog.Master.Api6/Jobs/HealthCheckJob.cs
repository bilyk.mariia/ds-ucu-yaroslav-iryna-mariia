﻿using Grpc.Core;
using Grpc.Net.ClientFactory;
using MediatR;
using Quartz;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.Master.Api6.Commands;
using ReplicatedLog.Master.Api6.Settings;
using ReplicatedLogGrcp;
using System.Configuration;

namespace ReplicatedLog.Master.Api6.Jobs
{
    public class HealthCheckJob: IJob
    {
        private readonly IConfiguration configuration;
        private readonly GrpcClientFactory grpcClientFactory;
        private readonly ISecondariesHealthStorage secondariesHealthStorage;
        private readonly ILogger<HealthCheckJob> logger;
        private readonly IMediator mediator;

        public HealthCheckJob(IConfiguration configuration, GrpcClientFactory grpcClientFactory, 
            ISecondariesHealthStorage secondariesHealthStorage, ILogger<HealthCheckJob> logger,
            IMediator mediator)
        {
            this.configuration = configuration;
            this.grpcClientFactory = grpcClientFactory;
            this.secondariesHealthStorage = secondariesHealthStorage;
            this.logger = logger;
            this.mediator = mediator;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var secondariesSettings = configuration
                .GetSection(SettingsConstants.ReplicationSecondariesSettingsName)
                .Get<List<ReplicationSecondarySettings>>();

            foreach (var secondary in secondariesSettings)
            {
                var client = grpcClientFactory.CreateClient<Health.HealthClient>(
                    secondary.Name + "_health");
                try
                {
                    var respose = await client.CheckAsync(new HealthCheckRequest(), deadline: DateTime.UtcNow.AddSeconds(5));
                    var wasHealthy = secondariesHealthStorage.IsHealthy(secondary.Name);
                    var isNowHealthy = respose.Status == HealthCheckResponse.Types.ServingStatus.Serving;
                    secondariesHealthStorage.Append(new Domain.SecondaryHealth
                    {
                        SecondaryName = secondary.Name,
                        IsHealthy = isNowHealthy,
                        LastCheck = DateTime.UtcNow
                    });

                    if (wasHealthy != isNowHealthy)
                    {
                        logger.LogWarning($"Secondary {secondary.Name} changed it's status to {respose.Status}");

                        if (isNowHealthy) _ = mediator.Send(new CompleteReplicationCommand(secondary.Name));

                    }
                } catch(RpcException ex) when (ex.StatusCode == StatusCode.DeadlineExceeded)
                {
                    logger.LogWarning($"Secondary {secondary.Name} changed it's status to UNRESPONSIVE");
               
                    secondariesHealthStorage.Append(new Domain.SecondaryHealth
                    {
                        SecondaryName = secondary.Name,
                        IsHealthy = false,
                        LastCheck = DateTime.UtcNow
                    });
            }
            }
        }
    }
}
