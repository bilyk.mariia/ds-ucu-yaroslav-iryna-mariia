using ReplicatedLog.Master.Api6.Settings;
using ReplicatedLog.Master.Api6.Services;
using ReplicatedLog.Master.Api6.Services.Interfaces;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.BuildingBlocks.Storage.InMemory;
using ReplicatedLogGrcp;
using System.Reflection;
using MediatR;
using ReplicatedLog.Master.Api6.Jobs;
using Quartz;
using Grpc.Net.Client.Configuration;
using Grpc.Net.Client;
using Grpc.Core;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var secondariesSettings = builder.Configuration.GetSection(SettingsConstants.ReplicationSecondariesSettingsName)
    .Get<List<ReplicationSecondarySettings>>();

var defaultMethodRetryConfig = new MethodConfig
{
    Names = { MethodName.Default },
    RetryPolicy = new RetryPolicy
    {
        MaxAttempts = 5,
        InitialBackoff = TimeSpan.FromSeconds(1),
        MaxBackoff = TimeSpan.FromSeconds(5),
        BackoffMultiplier = 1.5,
        RetryableStatusCodes = { StatusCode.Unavailable }
    }
};

var defaultMethodNoRetryConfig = new MethodConfig
{
    Names = { MethodName.Default },
    RetryPolicy = new RetryPolicy
    {
        MaxAttempts = 0
    }
};
foreach (var replicationSecondary in secondariesSettings)
{
    builder.Services.AddGrpcClient<MessageReplication.MessageReplicationClient>(replicationSecondary.Name, o =>
        {
           o.Address = new Uri(replicationSecondary.WriteEndpoint);
            o.ChannelOptionsActions.Add(p => new GrpcChannelOptions
            {
                ServiceConfig = new ServiceConfig { MethodConfigs = { defaultMethodRetryConfig } }
            });
    });

    builder.Services.AddGrpcClient<Health.HealthClient>(replicationSecondary.Name + "_health", o =>
    {
        o.Address = new Uri(replicationSecondary.WriteEndpoint);
        o.ChannelOptionsActions.Add(p => new GrpcChannelOptions
        {
            ServiceConfig = new ServiceConfig { MethodConfigs = { defaultMethodNoRetryConfig } }
        });
    });
}

var job = JobBuilder.Create<HealthCheckJob>()
    .WithIdentity(name: "HealthCheckJob", group: "HealthCheck")
    .Build();


var trigger = TriggerBuilder.Create()
    .WithIdentity(name: "HealthChecktrigger", group: "HealthCheckTriggers")
    .WithSimpleSchedule(o => o
        .RepeatForever()
        .WithIntervalInSeconds(10))
    .Build();

builder.Services.AddQuartz(q => q
    .UseMicrosoftDependencyInjectionScopedJobFactory(options => options.AllowDefaultConstructor = true));

builder.Services.AddQuartzHostedService(opt =>
{
    opt.WaitForJobsToComplete = true;
});

builder.Services.AddSingleton<IMessageStorage, MessageStorage>();
builder.Services.AddSingleton<ISecondariesHealthStorage, SecondariesHealthStorage>();
builder.Services.AddSingleton<IMessageReplicationStatusStorage, MessageReplicationStatusStorage>();

builder.Services.AddTransient<HealthCheckJob>();
builder.Services.AddTransient<ISaveMessageService, SaveMessageService>();
builder.Services.AddTransient<ISendMessageToSecondariesService, SendMessageToSecondariesService>();

builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

builder.Services.AddControllers();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
}

app.UseAuthorization();

app.MapControllers();

var schedulerFactory = app.Services.GetRequiredService<ISchedulerFactory>();
var scheduler = await schedulerFactory.GetScheduler();
await scheduler.ScheduleJob(job, trigger);

app.Run();
