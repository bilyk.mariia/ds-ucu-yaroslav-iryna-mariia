﻿using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.ApiModels;

namespace ReplicatedLog.Master.Api6.Services.Interfaces
{
    public interface ISaveMessageService
    {
        Task<Message> SaveMessageAsync(string messageText);
    }
}