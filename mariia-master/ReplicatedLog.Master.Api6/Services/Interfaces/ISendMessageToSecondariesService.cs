﻿using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.Settings;

namespace ReplicatedLog.Master.Api6.Services.Interfaces
{
    public interface ISendMessageToSecondariesService
    {
        Task<bool> SendMessageToSecondaries(Message message, WriteConcerns writeConcern, List<ReplicationSecondarySettings> secondariesSettings);
    }
}