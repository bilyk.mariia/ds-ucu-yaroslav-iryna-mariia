﻿using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.ApiModels;
using ReplicatedLog.Master.Api6.Services.Interfaces;

namespace ReplicatedLog.Master.Api6.Services
{
    public class SaveMessageService : ISaveMessageService
    {
        private readonly IMessageStorage _messageStorage;

        public SaveMessageService(IMessageStorage messageStorage)
        {
            this._messageStorage = messageStorage;
        }

        public async Task<Message> SaveMessageAsync(string messageText)
        {
            var lastItemAppended = _messageStorage.GetLast();
            var fullModel = new Message
            {
                Id = lastItemAppended?.Id + 1 ?? 1,
                MessageText = messageText,
                ReceivedAt = DateTime.UtcNow
            };

            _messageStorage.Append(fullModel);

            return fullModel;
        }
    }
}
