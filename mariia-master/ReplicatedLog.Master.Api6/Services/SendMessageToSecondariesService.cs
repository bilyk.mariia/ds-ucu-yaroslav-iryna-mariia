﻿using Google.Protobuf.WellKnownTypes;
using MediatR;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api6.Commands;
using ReplicatedLog.Master.Api6.Services.Interfaces;
using ReplicatedLog.Master.Api6.Settings;
using ReplicatedLogGrcp;

namespace ReplicatedLog.Master.Api6.Services
{
    public class SendMessageToSecondariesService : ISendMessageToSecondariesService
    {
        private readonly IMediator mediator;
        private readonly ILogger<SendMessageToSecondariesService> logger;

        public SendMessageToSecondariesService(IMediator mediator, ILogger<SendMessageToSecondariesService> logger)
        {
            this.mediator = mediator;
            this.logger = logger;
        }

        public async Task<bool> SendMessageToSecondaries(Message message, WriteConcerns writeConcern,
          List<ReplicationSecondarySettings> secondariesSettings)
        {
            logger.LogInformation($"SendMessageToSecondaries started for secondaries " +
                $"{string.Join(", ", secondariesSettings.Select(i => i.Name))}.");

            var messageRequestModel = new AppendMessageRequest()
            {
                Id = message.Id,
                MessageText = message.MessageText,
                CreatedAt = Timestamp.FromDateTime(message.ReceivedAt)
            };

            var secondariesRequestsCommands = secondariesSettings
                .Select(settings => new ReplicateMessageCommand(messageRequestModel, settings))
                .ToList();

            switch (writeConcern)
            {
                case WriteConcerns.MasterOnly:
                    {
                        foreach (var command in secondariesRequestsCommands)
                            _ = mediator.Send(command);
                        return true;
                    }

                case WriteConcerns.Majority:
                    {
                        var secondariesRequests = secondariesRequestsCommands.Select(command => mediator.Send(command)).ToList();

                        var totalReceived = new List<MessageReplicationStatus>();
                        while (secondariesRequests.Count > totalReceived.Count)
                        {
                            Task<MessageReplicationStatus> finishedTask = await Task.WhenAny(secondariesRequests);
                            secondariesRequests.Remove(finishedTask);
                            totalReceived.Add(await finishedTask);
                        }

                        logger.LogInformation($"Message with id {message.Id} replicated to majority of replicas.");
                        return totalReceived.All(r => r.ReplicatedSuccess);
                    }
                case WriteConcerns.All:
                default:
                    {
                        logger.LogInformation($"SendMessageToSecondaries processing commands " +
                            $"for secondaries: {string.Join(", ", secondariesRequestsCommands.Select(c => c.SecondarySettings.Name))}" +
                            $"for messages with ids: {string.Join(", ", secondariesRequestsCommands.Select(c => c.MessageRequestModel.Id))}.");
                        var result = await Task.WhenAll(secondariesRequestsCommands.Select(async command => await mediator.Send(command))) ;

                        return result.All(r => r.ReplicatedSuccess);
                    }
            }
        }
    }
}
