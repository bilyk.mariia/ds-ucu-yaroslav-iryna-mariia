﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReplicatedLog.Master.Api6.Settings;
public class ReplicationSecondarySettings
{
    public string Name { get; set; }
    public string WriteEndpoint { get; set; }
}
