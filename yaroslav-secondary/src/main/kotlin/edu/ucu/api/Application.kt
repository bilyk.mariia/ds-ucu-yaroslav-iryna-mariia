package edu.ucu.api

import io.micronaut.runtime.Micronaut

object ApplicationKt {
	@JvmStatic
	fun main(args: Array<String>) {
		var a = Micronaut.build()
			.packages("edu.ucu")
			.mainClass(ApplicationKt.javaClass)
			.start()
		a
	}
}

