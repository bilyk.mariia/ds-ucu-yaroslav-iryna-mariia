package edu.ucu.api.controllers.grpc.health

import ReplicatedLogGrcp.HealthGrpcKt
import ReplicatedLogGrcp.MessageOuterClass
import edu.ucu.infrastructure.health.HealthMonitor
import jakarta.inject.Singleton

@Singleton
class HealthController(private val healthMonitor: HealthMonitor): HealthGrpcKt.HealthCoroutineImplBase() {
    override suspend fun check(request: MessageOuterClass.HealthCheckRequest): MessageOuterClass.HealthCheckResponse {
        return MessageOuterClass.HealthCheckResponse.newBuilder()
            .setStatus(healthMonitor.servingStatus())
            .build()
    }
}