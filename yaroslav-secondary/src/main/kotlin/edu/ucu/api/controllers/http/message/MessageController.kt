package edu.ucu.api.controllers.http.message

import ReplicatedLogGrcp.MessageOuterClass.*
import edu.ucu.application.message.getall.GetAllMessagesQuery
import edu.ucu.application.message.getall.GetAllMessagesQueryHandler
import edu.ucu.application.message.getall.GetAllMessagesResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get


@Controller("/messages")
class MessageController(
    private val getAllMessagesQueryHandler: GetAllMessagesQueryHandler
) {
    @Get
    fun getAll(): GetAllMessagesResponse {
        return getAllMessagesQueryHandler.execute(GetAllMessagesQuery())
    }
}