package edu.ucu.common.date

import com.google.protobuf.Timestamp
import java.time.LocalDateTime

fun LocalDateTime.toTimestamp(): Timestamp {
    return Timestamp.newBuilder()
        .setNanos(this.nano)
        .setSeconds(this.second.toLong())
        .build()
}