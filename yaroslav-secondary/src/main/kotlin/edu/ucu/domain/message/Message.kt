package edu.ucu.domain.message

import java.time.LocalDateTime

class Message : Comparable<Message> {
    private val id: Long
    private val messageText: String
    private val createdAt: LocalDateTime

    fun id() = id
    fun messageText() = messageText
    fun createdAt() = createdAt

    private constructor(
        id: Long,
        messageText: String,
        createdAt: LocalDateTime,
    ) {
        this.id = id
        this.messageText = messageText
        this.createdAt = createdAt
    }

    companion object {
        fun create(id: Long, messageText: String, createdAt: LocalDateTime): Message {
            return Message(id, messageText, createdAt)
        }
    }

    override fun compareTo(other: Message): Int {
        if (id != other.id) return id.compareTo(other.id)
        if (createdAt != other.createdAt) return createdAt.compareTo(other.createdAt)
        if (messageText != other.messageText) return messageText.compareTo(other.messageText)
        return 0
    }
}