package edu.ucu.infrastructure.health

import io.micronaut.grpc.server.GrpcEmbeddedServer
import jakarta.inject.Inject
import jakarta.inject.Provider
import jakarta.inject.Singleton

@Singleton
class HealthMonitor {
    private lateinit var server: Provider<GrpcEmbeddedServer>

    @Inject
    fun setServer(server: Provider<GrpcEmbeddedServer>) {
        this.server = server
    }

    fun servingStatus(): ReplicatedLogGrcp.MessageOuterClass.HealthCheckResponse.ServingStatus {
        var isServing = server.get().isRunning
        if (isServing) return ReplicatedLogGrcp.MessageOuterClass.HealthCheckResponse.ServingStatus.SERVING
        else return ReplicatedLogGrcp.MessageOuterClass.HealthCheckResponse.ServingStatus.NOT_SERVING
    }
}