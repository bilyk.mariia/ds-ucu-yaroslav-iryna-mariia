package edu.ucu.infrastructure.inmemory

import edu.ucu.application.message.MessageRepository
import edu.ucu.domain.message.Message
import jakarta.inject.Singleton
import java.util.*

@Singleton
class MessageRepositoryImpl : MessageRepository {
    private val messages: MutableSet<Message> = TreeSet()

    override fun add(message: Message): Unit {
        messages.add(message)
    }

    override fun getAll(): Set<Message> {
        return Collections.unmodifiableSet(messages)
    }
}